﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDP_Console_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            // Server
            var host = Dns.GetHostEntry(Dns.GetHostName());


            //IPAddress paredHostIP = IPAddress.Parse("192.61.190.99"); // registered IP address on Windows; client is 192.61.190.55
            //IPAddress paredHostIP = IPAddress.Parse("192.61.190.100"); // fake IP (host unreachable on the network)
            
            //IPAddress paredHostIP = IPAddress.Parse("127.0.0.1"); // 193.61.190.56 - original, one IP address
            //IPEndPoint HostIpEndPoint = new IPEndPoint(paredHostIP, 3013);
            //UdpClient udpClient = new UdpClient(HostIpEndPoint);
            
            UdpClient udpClient = new UdpClient();

                
            try
            {
                //Connect to the client
                //udpClient.Connect("::1", 3010); // NOT
                //udpClient.Connect("loopback2", 3010); // NOT OK - resolves to 127.0.0.1
                //udpClient.Connect("localhost", 3010); // NOT OK - resolves to 127.0.0.1
                //udpClient.Connect("127.0.0.1", 3010); // NOT OK - resolves to 127.0.0.1


                //udpClient.Connect("193.61.190.56", 3010); // NOT OK - resolves to 127.0.0.1

                //udpClient.Connect("172.17.227.242", 3010); // NOT OK - resolves to 127.0.0.1
                udpClient.Connect("172.17.59.113", 3010);


                //udpClient.Connect("192.61.190.99", 3010); // NOT OK - source == destination (192.61.190.99)
                // udpClient.Connect("193.61.190.56", 3010); // NOT OK - as above (193.61.190.55 this time)
                //udpClient.Connect("127.255.255.255", 3010); // NOT OK - as above (193.61.190.55 this time)


                do
                {
                    // Sends a message to the host to which you have connected.
                    Byte[] sendBytes = Encoding.ASCII.GetBytes("Hello from the Console Server!");

                    udpClient.Send(sendBytes, sendBytes.Length);

                    // Send UDP every 200ms
                    System.Threading.Thread.Sleep(200);

                    //Console.WriteLine("Data sent to the client successfully.");

                    //Console.WriteLine("Press any key to send another UDP packet.");
                    //Console.ReadKey();
                }
                while (1 == 1); //INFINITE LOOP / EXECUTION
            }
            catch (Exception e)
            {
                udpClient.Close();
                Console.WriteLine(e.ToString());
                Console.WriteLine("Press any key to exit the app.");
                Console.ReadKey();
            }

            udpClient.Close();
        }
    }
}
